/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var processInclude = __webpack_require__(1);

$(document).ready(function () {
    console.log("newletter.js Here!");
    // $('.subscription-feedback').text(data.response);
    //  $('.newsletter-result').removeClass('hidden');
    //  $('.subscription-feedback').addClass("success");
    // $('#newsletter-button').on('click', function (e) {
        // console.log("prova");
        $('form.newsletter-form').on('submit', function (e) {
            var $form = $(this);
            e.preventDefault();
            var url = $form.attr('data-url'); //control
            console.log(url);
            $form.spinner().start();
            $('form.newsletter-form').trigger('newsletter:submit', e);
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: $form.serialize(),
                success: function (data) {
                    console.log(data);
                    $form.spinner().stop();
                    if (!data.success) {
                        $('.subscription-feedback').text(data.response);
                        $('.newsletter-result').removeClass('hidden');
                         $('.subscription-feedback').addClass("failure");
                         //console.log(data.response);

                        // formValidation($form, data);
                    } else {
                        //window.location.href = data.redirectUrl;
                        $('.subscription-feedback').text(data.response);
                        $('.newsletter-result').removeClass('hidden');
                         $('.subscription-feedback').addClass("success");
                         //console.log(data.response);

                    }
                },
                error: function (err) {
                    if (err.responseJSON.redirectUrl) {
                      window.location.href = err.responseJSON.redirectUrl;
                      console.log(err);

                    }
                    $form.spinner().stop();
                }
            });
            return false;
        });

        // $('form.numberOfClients-form').on('click', function (e) {
        //     var $form = $(this);
        //     e.preventDefault();
        //     console.log("numberofClients! !!! here! from newsletter.js");
        //     var url = $form.attr('action');
        //     $form.spinner().start();
        //     $.ajax({
        //         url: url,
        //         type: 'get',
        //         dataType: 'json',
        //         data: $form.serialize(),
        //         success: function (data) {
        //             console.log(data);
        //             if (data.success) {
        //                 console.log(data);
        //                 alert('data.succes is true and operation has been done successfully');
        //                 // formValidation($form, data);
        //             } else {
        //                 alert('error,from ajax call,data.success is not true');
        //             }
        //         },
        //         error: function (err) {
        //             if (err.responseJSON) {
        //                 window.location.href = err.responseJSON.redirectUrl;
        //                 console.log(err);
        //                 $form.spinner().stop();


        //             }
        //             $form.spinner().stop();
        //         }
        //     });
        //     return false;
        // });
        processInclude(__webpack_require__(2));
        }
    );


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function (include) {
    if (typeof include === 'function') {
        include();
    } else if (typeof include === 'object') {
        Object.keys(include).forEach(function (key) {
            if (typeof include[key] === 'function') {
                include[key]();
            }
        });
    }
};


/***/ }),
/* 2 */
/***/ (function(module, exports) {

// 'use strict';
// //front-end script
// var formValidation = require('base/components/formValidation');

// module.exports = function () {
//     //  $('button#newsletter-button').on('click', function (e) {
//             // console.log("prova");
//             $('form.newsletter-form').on('submit', function (e) {
//                 console.log("Hellofromnewsletter.js");
//                 var $form = $(this);
//                 e.preventDefault(); //stops default behaviour of the submit
//                 var url = $form.attr('data-url');
//                 console.log(url);
//                 $form.spinner().start();    //starts loader
//                 $('form.newsletter-form').trigger('newsletter:submit', e);
//                 $.ajax({
//                     url: url,
//                     type: 'post',
//                     dataType: 'json',
//                     data: $form.serialize(), //grabs data from form(inputs value)-serializes in json
//                     success: function (data) {
//                         console.log(data);
//                         $form.spinner().stop();
//                         if (!data.success) {
//                             $('.subscription-feedback').text(data.response);
//                             $('.newsletter-result').removeClass('hidden');
//                              $('.subscription-feedback').addClass("failure");
                          
//                             // formValidation($form, data);
//                         } else {
//                             //window.location.href = data.redirectUrl;
//                             $('.subscription-feedback').text(data.response); //add message to isml template .text
//                             $('.newsletter-result').removeClass('hidden');
//                              $('.subscription-feedback').addClass("success");
                          
//                         }
//                     },
//                     error: function (err) {
//                         if (err.responseJSON.redirectUrl) {
//                           window.location.href = err.responseJSON.redirectUrl;
//                         //   console.log(err);

//                         }
//                         $form.spinner().stop();
//                     }
//                 });
//                 return false;
//             });
// };

/***/ })
/******/ ]);