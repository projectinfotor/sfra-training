'use strict'


var server = require('server');
var cache = require('*/cartridge/scripts/middleware/cache');
var stringUtils = require('dw/util/StringUtils');
var calendar = require('dw/util/Calendar');
var paymentMgr = require('dw/order/PaymentMgr');
var Transaction = require('dw/system/Transaction');
var list = require('dw/util/List');


server.get(
    'Show',
    server.middleware.https,
    function (req, res, next) {

        var paymentList=paymentMgr.getActivePaymentMethods();
        var i;
        
        var jsonResponse = {};
        jsonResponse.item=[];
        
        for(i = 0; i<paymentList.length; i++){
            var item = paymentList.get(i);
            var itemJson = {};
            itemJson.ID=item.ID;
            itemJson.UUID=item.UUID;
            itemJson.active=item.active;
            
           
            jsonResponse.item.push(itemJson);
        };


        res.json(jsonResponse);
        next();

    });
    //next(); Very important in JS,tells : Show this on front/client side! A MUST !!!


module.exports = server.exports();