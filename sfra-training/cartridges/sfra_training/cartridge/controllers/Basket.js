'use strict';
var server = require('server');
var cache = require('*/cartridge/scripts/middleware/cache');

 server.get(
    'Show', cache.applyDefaultCache,
    function (req, res, next) {
        var BasketMgr = require('dw/order/BasketMgr');
        var currentBasket = BasketMgr.getCurrentBasket();
        var CartModel = require('*/cartridge/models/cart');
        var basketModel = new CartModel(currentBasket);
        res.render('basket', basketModel);
        next();
    }
);




module.exports =  server.exports();