// 'use strict';

//Use the following for CSRF protection: add middleware in routes and hidden field on form
var server = require('server');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var newsletterHelpers = require('*/cartridge/scripts/helpers/newsletterHelpers');
var URLUtils = require('dw/web/URLUtils');

server.get(
    'Show',
    server.middleware.https,
    function (req, res, next) {
        var newsletterForm = server.forms.getForm('newsletter');
        newsletterForm.clear();

        res.render('/newsletter/newslettersignup', {
            actionUrl: dw.web.URLUtils.url('Newsletter-Create'),
            newsletterForm: newsletterForm
        });

        next();
    }
);

server.post('Create',
    server.middleware.https,
    function (req, res, next) {
        var newsletterHelpers = require('*/cartridge/scripts/helpers/newsletterHelpers');
        var newsletterForm = server.forms.getForm('newsletter'); // <--- what does it do 
        var firstname = newsletterForm.fname.htmlValue;
        var lastname = newsletterForm.lname.htmlValue;
        var email = newsletterForm.email.htmlValue;
        var calendar = require('dw/util/Calendar');
        var customObjectMgr = require('dw/object/CustomObjectMgr');
        var Transaction = require('dw/system/Transaction');
        var stringUtils = require('dw/util/StringUtils');

        var service = newsletterHelpers.getService('crm.newsletter.subscribe');
        var requestObject = {
            properties: {
                firstname: firstname,
                lastname: lastname,
                email: email,

            }
        };



        var responseObject = service.call(requestObject);
        //uncomment this and then comment the Hook
        // var responseObject={
        //     ok: false
        // };
        var jsonResponse = {};

        if (responseObject.ok) {

            jsonResponse = {
                firstname: firstname,
                lastname: lastname,
                email: email,
                id: JSON.parse(responseObject.object).id,
                success: true,
                redirectUrl: dw.web.URLUtils.url('Newsletter-Success').toString(),
                response: responseObject.msg
            };
        } else {
            var currentTimeStamp = stringUtils.formatCalendar(new calendar(new Date()), "yyyyMMddHHmmssSS");


            Transaction.wrap(function () {
                var customObject = customObjectMgr.createCustomObject("userIfHubspotError", currentTimeStamp);
                customObject.custom.firstname = firstname;
                customObject.custom.lastname = lastname;
                customObject.custom.email = email;

            });

            var jobExHook = dw.system.HookMgr.callHook('app.jobExHook', 'jobExHook');

            jsonResponse = {
                response: "You account will be validated as soon as possible. Status:" + responseObject.msg,
                success: false
            };
        }

        res.json(jsonResponse);
        next(); //esponi in fron-end
    }
);


server.get(
    'List',
    server.middleware.https,
    function (req, res, next) {
        var newsletterHelpersList = require('*/cartridge/scripts/helpers/newsletterHelpersList');
        var stringUtils = require('dw/util/StringUtils');
        var newsletterListForm = server.forms.getForm('newsletter');//questo cosa fa
        var serviceDelete= newsletterHelpersList.deleteServiceList('crm.newsletter.subscribeDelete');
        var idOfClient=req.querystring.deleteButton;
        var numberOfClients=req.querystring.numberToDisplay ;

        if(req.querystring.deleteButton){
            var responseObjectOne =serviceDelete.call(idOfClient);
        }

        var service = newsletterHelpersList.getServiceList('crm.newsletter.subscribeList');

        if(numberOfClients){
            var responseObject= service.call(numberOfClients);
        } else {
            var responseObject=service.call();
        }


        var objectList=JSON.parse(responseObject.object).results;

        res.render('/newsletter/newsletterList', {
            objectList: objectList
        });

        next();
    }
);


server.get(
    'Success',
    server.middleware.https,
    function (req, res, next) {
        res.render('/newsletter/newslettersuccess', {
            continueUrl: URLUtils.url('Newsletter-Show'),
            newsletterForm: server.forms.getForm('newsletter')
        });

        next();
    }
);

server.post(
    'Handler',
    server.middleware.https,
    csrfProtection.validateAjaxRequest,
    function (req, res, next) {
        var newsletterForm = server.forms.getForm('newsletter');
        var continueUrl = dw.web.URLUtils.url('Newsletter-Show');

        // Perform any server-side validation before this point, and invalidate form accordingly
        if (newsletterForm.valid) {
            // Send back a success status, and a redirect to another route
            res.json({
                success: true,
                redirectUrl: URLUtils.url('Newsletter-Success').toString()
            });
        } else {
            // Handle server-side validation errors here: this is just an example
            res.setStatusCode(500);
            res.json({
                error: true,
                redirectUrl: URLUtils.url('Error-Start').toString()
            });
        }
        next();
    }
);

module.exports = server.exports();