'use strict';


var server = require('server');
var cache = require('*/cartridge/scripts/middleware/cache');
var stringUtils = require('dw/util/StringUtils');
var calendar = require('dw/util/Calendar');
var customObjectMgr = require('dw/object/CustomObjectMgr');
var Transaction = require('dw/system/Transaction');


server.get(
    'Create', //in URl : CustomObject-Create
    server.middleware.https,
    function (req, res, next) {

        var currentTimeStamp = stringUtils.formatCalendar(new calendar(new Date()), "yyyyMMddHHmmssSS");
        var objectId = {};
        //if null!!!!
        if(dw.system.Site.getCurrent().getCustomPreferenceValue("createObject") != null ){
            objectId.name = req.querystring.name;
            objectId.surname = req.querystring.surname;
            objectId.email = req.querystring.email;

            //trans for object handling
            Transaction.wrap(function () {
                var customObject = customObjectMgr.createCustomObject( dw.system.Site.getCurrent().getCustomPreferenceValue("newsletterObjectName"), currentTimeStamp);

                customObject.custom.nameAttr = objectId.name;
                customObject.custom.surname = objectId.surname;
                customObject.custom.email = objectId.email;
                customObject.custom.timeStamp = currentTimeStamp;

                var jsonResponse = {};
                jsonResponse.success = true;
                jsonResponse.idcust = customObject.custom.timeStamp; //my  timeStamp ID in Bm
                jsonResponse.customObject = customObject.custom.nameAttr;

                res.json(jsonResponse);
                next();
            });
        } else {
            jsonError={};
            jsonError.success = false;
            res.json(jsonError);
            next();
        }
        });





server.get(
    'Show',
    server.middleware.https,
    function (req, res, next) {
        var currentTimeStamp = stringUtils.formatCalendar(new calendar(new Date()), "yyyyMMddHHmmssSS");
        var customObject = customObjectMgr.getAllCustomObjects( dw.system.Site.getCurrent().getCustomPreferenceValue("newsletterObjectName"));

        var jsonResponse = {};
        jsonResponse.item = [];

        while (customObject.hasNext()) {
            var item = customObject.next();
            var itemJson = {};
            itemJson.timeStamp = item.custom.nameAttr;
            itemJson.firstname = item.custom.surname;
            itemJson.email = item.custom.email

            jsonResponse.item.push(itemJson);
        }

        //jsonResponse.count = customObject.count;

        res.json(jsonResponse);
        next();

    });



server.get(
    'Delete',
    server.middleware.https,
    function (req, res, next) {

        var currentTimeStamp = stringUtils.formatCalendar(new calendar(new Date()), "yyyyMMddHHmmssSS");
        var id = req.querystring.id;
        var customObject = customObjectMgr.getCustomObject( dw.system.Site.getCurrent().getCustomPreferenceValue("newsletterObjectName"), id);

        //Transaction to handle objects,apre una persistenza nella database,apro una connessione verso i database per modificare il ogetto
        Transaction.wrap(function () {
        customObjectMgr.remove(customObject);
        });


        var jsonResponse = {};
       jsonResponse.id = id;

        res.json(jsonResponse);
        next();
        //next(); is a must!!!!! Esponi  tutto in front-End


    }
);


//console log error ? res.render(error)
//server get ? Perche non Post ? -because we call a other




module.exports = server.exports();