'use strict';

var processInclude = require('base/util');

$(document).ready(function () {
    console.log("newletter.js Here!");
    // $('.subscription-feedback').text(data.response);
    //  $('.newsletter-result').removeClass('hidden');
    //  $('.subscription-feedback').addClass("success");
    // $('#newsletter-button').on('click', function (e) {
        // console.log("prova");
        $('form.newsletter-form').on('submit', function (e) {
            var $form = $(this);
            e.preventDefault();
            var url = $form.attr('data-url'); //control
            console.log(url);
            $form.spinner().start();
            $('form.newsletter-form').trigger('newsletter:submit', e);
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: $form.serialize(),
                success: function (data) {
                    console.log(data);
                    $form.spinner().stop();
                    if (!data.success) {
                        $('.subscription-feedback').text(data.response);
                        $('.newsletter-result').removeClass('hidden');
                         $('.subscription-feedback').addClass("failure");
                         //console.log(data.response);

                        // formValidation($form, data);
                    } else {
                        //window.location.href = data.redirectUrl;
                        $('.subscription-feedback').text(data.response);
                        $('.newsletter-result').removeClass('hidden');
                         $('.subscription-feedback').addClass("success");
                         //console.log(data.response);

                    }
                },
                error: function (err) {
                    if (err.responseJSON.redirectUrl) {
                      window.location.href = err.responseJSON.redirectUrl;
                      console.log(err);

                    }
                    $form.spinner().stop();
                }
            });
            return false;
        });

        // $('form.numberOfClients-form').on('click', function (e) {
        //     var $form = $(this);
        //     e.preventDefault();
        //     console.log("numberofClients! !!! here! from newsletter.js");
        //     var url = $form.attr('action');
        //     $form.spinner().start();
        //     $.ajax({
        //         url: url,
        //         type: 'get',
        //         dataType: 'json',
        //         data: $form.serialize(),
        //         success: function (data) {
        //             console.log(data);
        //             if (data.success) {
        //                 console.log(data);
        //                 alert('data.succes is true and operation has been done successfully');
        //                 // formValidation($form, data);
        //             } else {
        //                 alert('error,from ajax call,data.success is not true');
        //             }
        //         },
        //         error: function (err) {
        //             if (err.responseJSON) {
        //                 window.location.href = err.responseJSON.redirectUrl;
        //                 console.log(err);
        //                 $form.spinner().stop();


        //             }
        //             $form.spinner().stop();
        //         }
        //     });
        //     return false;
        // });
        processInclude(require('./newsletter/newsletter'));
        }
    );
