'use strict'

var Status = require('dw/system/Status');
var stringUtils = require('dw/util/StringUtils');
var calendar = require('dw/util/Calendar');
var customObjectMgr = require('dw/object/CustomObjectMgr');
var Transaction = require('dw/system/Transaction');
var Logger = require('dw/system/Logger');
var customLogger = Logger.getLogger("NewsletterJobLogger", "error");

function jobEx() {
    var args = arguments[0];
    var customObject = {};
    customObject.type = args.CustomObjectType;
    var newsletterHelpers = require('*/cartridge/scripts/helpers/newsletterHelpers');
    var objectToResend = customObjectMgr.getAllCustomObjects(customObject.type);
    var service = newsletterHelpers.getService('crm.newsletter.subscribe');


    while (objectToResend.hasNext()) {
        var item = objectToResend.next();


        var objectToUpload = {
            properties: {
                firstname: item.custom.firstname,
                lastname: item.custom.lastname,
                email: item.custom.email
            }
        };

        var responseObject = service.call(objectToUpload);

//Contant already exists
        if (responseObject.ok) {
             customLogger.info("Hello from JobEx,Petru Tagarcea");
            Transaction.wrap(function () {
                customObjectMgr.remove(item);
            });
            return new Status(Status.OK);
        } else {
            customLogger.error("Error from JobEx.js");
            return new Status(Status.ERROR);
        }
    };
    //while finishes


};





exports.jobEx = jobEx;