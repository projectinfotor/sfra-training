'use strict'

var Status = require ('dw/system/Status');
var stringUtils = require('dw/util/StringUtils');
var calendar = require('dw/util/Calendar');
var customObjectMgr = require('dw/object/CustomObjectMgr');
var Transaction = require('dw/system/Transaction');

function JobTest() {
    var args = arguments[0];
    var customObject={};
    customObject.type=args.TestParam;
    customObject.key=args.testParam2;
    var objectToRemove =customObjectMgr.getAllCustomObject(customObject.type, customObject.key);

    if(objectToRemove != null){
        Transaction.wrap(function () {
                    customObjectMgr.remove(objectToRemove);
                    });

            return new Status(Status.OK);

        } else {
            return new Status(Status.ERROR);
        }

}

exports.JobTest=JobTest;