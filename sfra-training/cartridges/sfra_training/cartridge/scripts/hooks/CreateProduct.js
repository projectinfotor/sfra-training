'use strict';

function CreateProduct(product) {

    var calendar = require("dw/util/Calendar");
    var stringUtils = require('dw/util/StringUtils');

    var currentTimeStamp = stringUtils.formatCalendar(new calendar(new Date()), "yyyyMMddHHmmssSS");
    var customObject = {};
    dw.system.Transaction.wrap(function () {

        customObject = dw.object.CustomObjectMgr.createCustomObject("AcademyProducts", "Idproducttest9");
        customObject.custom.AcademyProductsName = product.pageTitle;
        customObject.custom.AcademyProductsDescription = product.shortDescription;

    });

    return customObject;

}

exports.CreateProduct = CreateProduct;
