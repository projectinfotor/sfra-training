'use strict';

var collections = require('*/cartridge/scripts/util/collections');
var ShippingLocation = require('dw/order/ShippingLocation');
var customObjectMgr = require('dw/object/CustomObjectMgr');
var Transaction = require('dw/system/Transaction');

function jobExHook() {

    var newsletterHelpers = require('*/cartridge/scripts/helpers/newsletterHelpers');
    var objectToResend = customObjectMgr.getAllCustomObjects("userIfHubspotError");
    var service = newsletterHelpers.getService('crm.newsletter.subscribe');

    while (objectToResend.hasNext()) {
        var item = objectToResend.next();


        var objectToUpload = {
            properties: {
                firstname: item.custom.firstname,
                lastname: item.custom.lastname,
                email: item.custom.email
            }
        };
        var response = service.call(objectToResend);


        if (response.ok) {
            Transaction.wrap(function () {
                customObjectMgr.remove(item);
            });
        } else {
            return false;
        }
    }
    return true;

};



exports.jobExHook = jobExHook;