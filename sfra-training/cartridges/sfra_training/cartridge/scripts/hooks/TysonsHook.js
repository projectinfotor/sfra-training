'use strict'

var collections = require('*/cartridge/scripts/util/collections');
var ShippingLocation = require('dw/order/ShippingLocation');
var ProductMgr = require('dw/catalog/ProductMgr');
var customObjectMgr = require('dw/object/CustomObjectMgr');
var Transaction = require('dw/system/Transaction');


function StringCall() {
    var IdOfProduct="25752986M";
    var product = ProductMgr.getProduct(IdOfProduct);
    var productDetails = {};
    productDetails.ID=product.ID;
    productDetails.pageTitle=product.pageTitle;
    productDetails.pageDescription=product.pageDescription;

        return productDetails;
}

exports.StringCall = StringCall;

