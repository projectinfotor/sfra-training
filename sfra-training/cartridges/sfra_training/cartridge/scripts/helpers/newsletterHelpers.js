'use strict';
// code version sfrademo-creating.
// sfra-training at the top

var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var HTTPClient = require('dw/net/HTTPClient');
var HTTPService = require('dw/svc/HTTPService');
/**
 * Fetches the local service registry assigned to a service id
 * @param {String} serviceId- service id
 * @returns {Object} a local service registry
 */
function getService(serviceId) {

    return LocalServiceRegistry.createService(serviceId, {
        createRequest: function(svc: HTTPService, args) {

            if (args) {

                // svc.addParam("hapikey","64155f4e-3c4a-49ee-8c86-7b07b622eaa5");
                svc.URL=svc.URL + dw.system.Site.getCurrent().getCustomPreferenceValue("hapikey");
                // svc.requestMethod = "POST";
                svc.addHeader("Content-Type", "application/json");
                return JSON.stringify(args);
            } else {
                return null;
            }
        },

        parseResponse: function(svc: HTTPService, client: HTTPClient) {
            return client.text;
        },

        mockCall: function(svc: HTTPService, client: HTTPClient) {

                    return {
                        statusCode: 200,
                        statusMessage: "Success",
                        text: "MOCK RESPONSE (" + svc.URL + ")"
                    };
                }
    });
};
module.exports = {
    getService: getService
};
